package br.edu.up.listademusicas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

public class CapasAdapter extends BaseAdapter {

  private Context contexto;
  private List<Musica> lista;

  public CapasAdapter(Context contexto, List<Musica> lista) {
    this.contexto = contexto;
    this.lista = lista;
  }

  @Override
  public int getCount() {
    return lista.size();
  }

  @Override
  public Object getItem(int i) {
    return lista.get(i);
  }

  @Override
  public long getItemId(int i) {
    return lista.get(i).getIdMusica();
  }

  @Override
  public View getView(int i, View view, ViewGroup root) {

    Musica musica = lista.get(i);

    LayoutInflater inflater = LayoutInflater.from(contexto);
    View item = inflater.inflate(R.layout.item_da_grid, root, false);

    ImageView iconeCapa = (ImageView) item.findViewById(R.id.iconeCapa);
    iconeCapa.setImageResource(musica.getIdCapa());

    return item;
  }
}