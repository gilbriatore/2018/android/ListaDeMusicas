package br.edu.up.listademusicas;

import java.util.ArrayList;

public class CadastroDeMusicas extends ArrayList<Musica> {

    public CadastroDeMusicas(){

      Musica musica1 = new Musica();
      musica1.setArtista("Anitta");
      musica1.setNome("Bang");
      musica1.setAlbum("Bang");
      musica1.setAno("2015");
      musica1.setIdCapa(R.drawable.anitta);
      musica1.setIdMusica(R.raw.anitta_bang);

      add(musica1);

      Musica musica2 = new Musica();
      musica2.setArtista("Jota Quest");
      musica2.setNome("Na moral");
      musica2.setAlbum("Discotecagem Pop Variada");
      musica2.setAno("2002");
      musica2.setIdCapa(R.drawable.jota_quest);
      musica2.setIdMusica(R.raw.jota_quest_na_moral);

      add(musica2);

      Musica musica3 = new Musica();
      musica3.setArtista("Pink Floyd");
      musica3.setNome("Another Brick in the Wall");
      musica3.setAlbum("The Wall");
      musica3.setAno("1979");
      musica3.setIdCapa(R.drawable.pink_floyd);
      musica3.setIdMusica(R.raw.pink_floyd_another_brick_in_the_wall_p2);

      add(musica3);

      Musica musica4 = new Musica();
      musica4.setArtista("Sambô");
      musica4.setNome("Sunday Bloody Sunday");
      musica4.setAlbum("Estação Sambô - Ao Vivo");
      musica4.setAno("2012");
      musica4.setIdCapa(R.drawable.sambo);
      musica4.setIdMusica(R.raw.sambo_sunday_bloody_sunday);

      add(musica4);

      Musica musica5 = new Musica();
      musica5.setArtista("Talking Heads");
      musica5.setNome("Psycho Killer");
      musica5.setAlbum("Talking Heads: 77");
      musica5.setAno("1977");
      musica5.setIdCapa(R.drawable.talking_heads);
      musica5.setIdMusica(R.raw.talking_heads_psycho_killer);

      add(musica5);

      Musica musica6 = new Musica();
      musica6.setArtista("The Doors");
      musica6.setNome("Light My Fire");
      musica6.setAlbum("The Doors");
      musica6.setAno("1967");
      musica6.setIdCapa(R.drawable.the_doors);
      musica6.setIdMusica(R.raw.the_doors_light_my_fire);

      add(musica6);

      Musica musica7 = new Musica();
      musica7.setArtista("Zeca Baleiro");
      musica7.setNome("Disritimia");
      musica7.setAlbum("Vô Imbolá");
      musica7.setAno("1999");
      musica7.setIdCapa(R.drawable.zeca_baleiro);
      musica7.setIdMusica(R.raw.zeca_baleiro_disritmia);

      add(musica7);
    }
}