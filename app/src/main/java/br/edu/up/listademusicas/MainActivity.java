package br.edu.up.listademusicas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    CadastroDeMusicas cadastroDeMusicas = new CadastroDeMusicas();

    MusicasAdapter musicasAdapter = new MusicasAdapter(this, cadastroDeMusicas);
    ListView listView = (ListView) findViewById(R.id.lstMusicas);
    listView.setAdapter(musicasAdapter);
    listView.setOnItemClickListener(this);

    CapasAdapter capasAdapter = new CapasAdapter(this, cadastroDeMusicas);
    GridView gridView = (GridView) findViewById(R.id.grdCapas);
    gridView.setAdapter(capasAdapter);
    gridView.setOnItemClickListener(this);
  }

  @Override
  public void onItemClick(AdapterView<?> adapterView, View view, int itemSelecionado, long l) {
    Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
    intent.putExtra("musicaSelecionada", itemSelecionado);
    startActivity(intent);
  }
}