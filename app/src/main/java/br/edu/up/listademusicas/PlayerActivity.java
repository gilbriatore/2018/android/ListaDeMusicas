package br.edu.up.listademusicas;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.ToggleButton;

import java.util.Random;

/**
 * @author Geucimar
 * @since 07/09/2016
 * @version 1.0
 */

public class PlayerActivity extends AppCompatActivity
    implements MediaPlayer.OnCompletionListener,
    View.OnTouchListener, CompoundButton.OnCheckedChangeListener {

  private int musicaSelecionada;
  private ImageButton btnPlay;
  private MediaPlayer player;
  private SeekBar barraDeProgresso;
  private ImageView capaAtual;
  private boolean tocarAleatorio = false;
  private CadastroDeMusicas cadastroDeMusicas;
  private Musica musicaAtual;

  /**
   * O QUE É
   * Método utilizado na criação da activity;
   *
   * QUANDO É CHAMADO
   * Chamado automaticamente pelo aplicativo na inicialialização;
   *
   * PARA QUE ESTA SENDO UTILIZADO
   * Está sendo utilizado para receber os dados, carregar os componentes
   * criados no XML e iniciar o toque da música selecionada.
   *
   * @param savedInstanceState parâmetros de estado da activity
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_player);

    cadastroDeMusicas = new CadastroDeMusicas();

    //Recebe os dados de outra activity;
    Intent intent = getIntent();
    musicaSelecionada = intent.getIntExtra("musicaSelecionada",0);

    //Carrega os componentes criados no xml;
    capaAtual = (ImageView) findViewById(R.id.imgCapa);
    barraDeProgresso = (SeekBar) findViewById(R.id.barraDeProgresso);
    barraDeProgresso.setOnTouchListener(this);
    btnPlay = (ImageButton) findViewById(R.id.btnPlay);
    ToggleButton btnAleatorio = (ToggleButton) findViewById(R.id.btnAleatorio);
    btnAleatorio.setOnCheckedChangeListener(this);

    //Começa a tocar a música;
    tocarMusica();
  }

  public void tocarMP3(View v){
    if (player == null) {
      tocarMusica();
    } else if(!player.isPlaying()){
      reiniciarMusica();
    } else {
      pausarMusica();
    }
  }

  private void reiniciarMusica() {
    if (player != null) {
      player.start();
      btnPlay.setImageResource(R.drawable.pause50px);
    }
  }

  private void pausarMusica() {
    if (player != null) {
      player.pause();
      btnPlay.setImageResource(R.drawable.play50px);
    }
  }

  private void tocarMusica() {

    limparPlayer();
    musicaAtual = cadastroDeMusicas.get(musicaSelecionada);
    capaAtual.setImageResource(musicaAtual.getIdCapa());
    btnPlay.setImageResource(R.drawable.pause50px);

    player = MediaPlayer.create(PlayerActivity.this, musicaAtual.getIdMusica());
    player.setOnCompletionListener(this);
    int tamanho = player.getDuration();
    barraDeProgresso.setMax(tamanho);
    player.start();
    atualizarProgresso();

  }

  private void atualizarProgresso() {

    if(player != null && player.isPlaying()) {

      int posicaoAtual = player.getCurrentPosition();
      barraDeProgresso.setProgress(posicaoAtual);
      Runnable processo = new Runnable() {
        @Override
        public void run() {
          atualizarProgresso();
        }
      };
      new Handler().postDelayed(processo, 1000);
    }
  }

  public void pararMP3(View v){
    limparPlayer();
  }

  private void limparPlayer() {
    if (player != null) {
      btnPlay.setImageResource(R.drawable.play50px);
      player.release();
      player = null;
    }
  }

  public void proximo(View v){
    selecionarMusica(1);
    tocarMusica();
  }

  public void voltar(View v){
    selecionarMusica(-1);
    tocarMusica();
  }

  @Override
  public void onCompletion(MediaPlayer mediaPlayer) {
    selecionarMusica(1);
    tocarMusica();
  }

  private void selecionarMusica(int incremento) {

    if(tocarAleatorio){
      Random r = new Random();
      musicaSelecionada = r.nextInt(cadastroDeMusicas.size() -1);
    } else {
      musicaSelecionada = musicaSelecionada + incremento;
    }

    if (musicaSelecionada > cadastroDeMusicas.size() - 1){
      musicaSelecionada = 0;
    }

    if (musicaSelecionada < 0){
      musicaSelecionada = cadastroDeMusicas.size() -1;
    }
  }

  @Override
  public boolean onTouch(View view, MotionEvent motionEvent) {
    SeekBar seekBar = (SeekBar) view;
    int progresso = seekBar.getProgress();
    player.seekTo(progresso);
    return false;
  }

  @Override
  public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
    tocarAleatorio = isChecked;
  }

  @Override
  protected void onPause() {
    super.onPause();
    pausarMusica();
  }

  @Override
  protected void onResume() {
    super.onResume();
    reiniciarMusica();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    limparPlayer();
  }
}