package br.edu.up.listademusicas;

import java.io.Serializable;

public class Musica implements Serializable {

  private int idMusica;
  private int idCapa;
  private String nome;
  private String artista;
  private String album;
  private String ano;

  public int getIdMusica() {
    return idMusica;
  }

  public void setIdMusica(int idMusica) {
    this.idMusica = idMusica;
  }

  public int getIdCapa() {
    return idCapa;
  }

  public void setIdCapa(int idCapa) {
    this.idCapa = idCapa;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getArtista() {
    return artista;
  }

  public void setArtista(String artista) {
    this.artista = artista;
  }

  public String getAlbum() {
    return album;
  }

  public void setAlbum(String album) {
    this.album = album;
  }

  public String getAno() {
    return ano;
  }

  public void setAno(String ano) {
    this.ano = ano;
  }
}
